# Responsive signature

Dockerize version of https://github.com/danmindru/responsive-html-email-signature#usage-with-different-email-clients

## TL;DR;

1. Create your template folder
1. run 
  ```bash
   docker run --rm \
     -v "$(pwd)/templates:/app/templates" \
     -v "$(pwd)/dist:/output" \
     -u "$(id -u):$(id -g)" \
     registry.gitlab.com/savadenn-public/responsive-signature
  ```
1. Open ./dist
1. Profit
