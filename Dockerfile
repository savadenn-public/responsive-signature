FROM node:latest

WORKDIR /app

RUN git clone https://github.com/danmindru/responsive-html-email-signature .

RUN chmod 777 /app

RUN yarn

CMD yarn once \
    && rm -rf /output/* \
    && cp -r /app/dist/* /output/