#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY: dev

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#-----------------------------------------
# Commands
#-----------------------------------------

clean: ## Cleans up environnement
	@docker-compose down --remove-orphans

dev: clean ## Starts dev stack
	@docker-compose build --pull
	@echo "\nConnect to https://${PROJECT_URL}\n"
	@docker-compose up
